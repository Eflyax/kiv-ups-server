#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/timeb.h>

#define MAX_CLIENTS  50
#define SEED_LENGTH  10
#define MAX_TEAM_ID  2

pthread_mutex_t send_m;
pthread_mutex_t receive_m;

int count_send_message;
int count_received_message;
    
int MAX_ROOMS = 30;
int PORT = 10042;

pthread_t handle_commander;
static unsigned int cli_count = 0;
static int uid = 10;
int last_game_id = 1; /* ID posledni zalozene hry */
char lobbyInfo[4096]; /* Info o založených hrách */
int client_socket[MAX_CLIENTS];
FILE *log_server; /* log soubory */

/* Client structure */
typedef struct Client {
    struct sockaddr_in addr; /* Client remote address */
    int connfd; /* Connection file descriptor */
    int uid; /* Client unique identifier */
    char name[15]; /* Client name */
    int game_id; /* Id hry, ve které hraje*/
    int leader; /* Vedouci lobby (zakladatel) */
    int game_state; /* Je v Lobby ? |  0=na serveru, 1=lobby  */
    int ping; /* zpozdeni klienta */
    int accepted;
    struct Client *next;
} client;

typedef struct Game {
    int game_id;

    int p1_socket;
    char p1_nick[96];
    int p1_team;
    int p1_ping;
    int p1_ready;
    int p1_hp;

    int p2_socket;
    char p2_nick[96];
    int p2_team;
    int p2_ping;
    int p2_ready;
    int p2_hp;

    char rejoin_string[4096]; /* popis aktuální hry, lze se podle něho vrátit zpět do hry */
    struct Game *next;
    char seed[SEED_LENGTH];
} game;

game *hra_struct;
struct Game *game_list;

client *client_struct;
struct Client *client_list;

/* tisk seznamu her */
void print_games(game *head, int socket) {

    char *zprava2 = malloc(sizeof (char)*100);
    memset(zprava2, 0, sizeof zprava2);
    sprintf(zprava2 + strlen(zprava2), "%s", "lobby;");

    while (head) {
	sprintf(zprava2 + strlen(zprava2), "%d", head->game_id);
	sprintf(zprava2 + strlen(zprava2), "%s", "#");
	sprintf(zprava2 + strlen(zprava2), "%s", head->p1_nick);
	sprintf(zprava2 + strlen(zprava2), "%s", "#");
	sprintf(zprava2 + strlen(zprava2), "%s", head->p2_nick);
	sprintf(zprava2 + strlen(zprava2), "%s", ";");
	head = head->next;
    }
    send(socket, zprava2, strlen(zprava2), 0);
    free(zprava2);

    char *zprava = malloc(sizeof (char)*100);
    sprintf(zprava, "%s\n", "");
    send(socket, zprava, strlen(zprava), 0);
    free(zprava);
}

void print_games_local(game *head) {

    printf("##########################################\n");
    printf("#######  Místnosti na serveru:  ##########\n");
    if (head != NULL) {
	while (head) {
	    printf("[Místnost %d] [%s] vs [%s]\n", head->game_id, head->p1_nick, head->p2_nick);
	    head = head->next;
	}
    } else {
	printf("###  Na serveru nejsou žádné místnosti ###\n");
    }
    printf("##########################################\n");
}

void print_clients_local(client *head) {

    if (head != NULL) {
	while (head) {
	    printf("%s\n", head->name);
	    head = head->next;
	}
    } else {
	printf(" [Prázdné]\n");
    }
}

/* odebrani hry (remove head) */
void list_remove_game(game **p) {
    if (p && *p) {
	game *n = *p;
	*p = (*p)->next;

	n->p1_ready = 0;
	n->p2_ready = 0;
	n->p1_socket = 0;
	n->p2_socket = 0;
	n->p1_team = 1;
	n->p2_team = 1;
	n->p2_hp = 500;
	n->p1_hp = 500;

	sprintf(n->rejoin_string, "%s", "");
	sprintf(n->seed, "%s", "");
	sprintf(n->p2_nick, "%s", "");
	sprintf(n->p1_nick, "%s", "");

	free(n);
	last_game_id--;
    }
}

void list_remove_client(client **p) {
    if (p && *p) {
	client *n = *p;
	*p = (*p)->next;
	sprintf(n->name, "%s", "");
	free(n);
    }
}

/* pridani nove hry do seznamu her */
game *add_game(game **head, int p1_socket, int p2_socket) {

    game *newgame, *p;

    /* inicializace nove hry */
    newgame = malloc(sizeof (game)); /* alokace pameti pro strukturu */
    newgame->game_id = last_game_id; /* ID hry */
    last_game_id++;
    newgame->p1_socket = p1_socket; /* socket 1. hrace */
    newgame->p1_team = 1; /* team 1. hrace */
    newgame->p1_ready = 0;
    newgame->p1_hp = 500;

    sprintf(newgame->p1_nick, "%s", "");

    newgame->p2_socket = p2_socket; /* socket 2. hrace */
    newgame->p2_team = 1; /* team 2. hrace */
    newgame->p2_ready = 0;
    newgame->p2_hp = 500;

    sprintf(newgame->p2_nick, "%s", "");

    time_t t;
    srand((unsigned) time(&t));
    char seed[SEED_LENGTH];
    int g, value;
    for (g = 0; g < SEED_LENGTH; g++) {
	value = 97 + (rand() % 25);
	if (value == ';') {
	    value++;
	}
	seed[g] = value;
    }
    strncpy(newgame->seed, seed + 0, SEED_LENGTH);

    /* zarazeni hry do seznamu her */
    if (*head) {
	p = *head;
	while (p->next) p = p->next;
	p->next = newgame;
    } else {
	*head = newgame;
    }
    return newgame;
}

/* pridani nove hry do seznamu her */
client *add_client(client **head, char * name) {

    client *newclient, *p;

    newclient = malloc(sizeof (client)); /* alokace pameti pro strukturu */
    sprintf(newclient->name, "%s", name); /* ID hry */
    /* zarazeni hry do seznamu her */
    if (*head) {
	p = *head;
	while (p->next) p = p->next;
	p->next = newclient;
    } else {
	*head = newclient;
    }
    return newclient;
}

/* nalezeni klienta podle jeho jmena */
game **list_search_game(game **game_list, int id) {
    if (game_list) {
	while (*game_list) {
	    if ((*game_list)->game_id == id) return game_list;
	    game_list = &(*game_list)->next;
	}
    }
    return NULL;
}

/* nalezeni klienta podle jeho jmena */
client **list_search_client(client **client_list, char *name) {
    if (client_list) {
	while (*client_list) {

	    if (!strcmp((*client_list)->name, name)) {
		return client_list;
	    }

	    client_list = &(*client_list)->next;
	}
    }
    return NULL;
}

/* nalezeni hry podle jejiho id (game_id) */
game *get_game_rejoin(game **seznam, char *name) {
    game *ptr;
    if (*seznam) {
	ptr = *seznam;
	while (ptr) {
	    if (!strcmp(ptr->p1_nick, name)) {
		if (ptr->p1_socket == 0) {
		    //ma rozehranou hru (leader)
		    return ptr;
		}
	    }
	    if (!strcmp(ptr->p2_nick, name)) {
		if (ptr->p2_socket == 0) {
		    //ma rozhranou hru (leader)
		    return ptr;
		}
	    }
	    ptr = ptr->next;
	}
    }
    return NULL;
}

/* nalezeni hry podle jejiho id (game_id) */
game *get_game(game **seznam, int id) {
    game *ptr;
    if (*seznam) {
	ptr = *seznam;
	while (ptr) {
	    if (ptr->game_id == id) {
		return ptr;
		break;
	    }
	    ptr = ptr->next;
	}
    }
    return NULL;
}

/* nalezeni hry podle jejiho id (game_id) */
client *get_client(client **seznam, char *name) {
    client *ptr;
    if (*seznam) {
	ptr = *seznam;
	while (ptr) {
	    if (!strcmp(client_list->name, name)) {
		return ptr;
		break;
	    }
	    ptr = ptr->next;
	}
    }
    return NULL;
}

game *get_game_null() {
    return NULL;
}

void sendToClient(char *s, int socket) {
    pthread_mutex_lock(&send_m);
    char *zprava = malloc(sizeof (char)*3000);
    sprintf(zprava, "%s\n", s);
    send(socket, zprava, strlen(zprava), 0);
    free(zprava);
    count_send_message++;
    pthread_mutex_unlock(&send_m);
}

/* Handle all communication with the client */
void *handle_client(void *arg) {
    //char buff_out[1024];
    char buff_in[1024];
    int rlen;
    client *cli = (client *) arg;
    game *join_game;
    client *search_client; // = malloc(client);


    log_server = fopen("server.log", "a+");
    fprintf(log_server, "Připojil se nový klient\n");
    fclose(log_server);


    struct timeb tmb;
    struct timeb cur_tmb;
    /* pripojil se novy klient, poslu mu zalozene hry */
    char* input[30];
    while ((rlen = recv(cli->connfd, buff_in, sizeof (buff_in) - 1, 0)) > 0) {

	buff_in[rlen] = '\0';

	if (!strlen(buff_in)) {
	    continue;
	}
	
	pthread_mutex_lock(&receive_m);
	count_received_message++;
	pthread_mutex_unlock(&receive_m);

	char *command;
	command = strtok(buff_in, ";");

	if (!strcmp("list", command)) {
	    print_games(game_list, cli->connfd);
	} else if (!strcmp("hi-c", command)) {
	    command = strtok(NULL, ";");
	    if (command == NULL) {
		strcpy(cli->name, "Player");
	    } else {
		strncpy(cli->name, command + 0, 15);
	    }
	    printf("Připojil se klient %s \n", cli->name);

	    log_server = fopen("server.log", "a+");
	    fprintf(log_server, "Klient se představil jako %s.\n", cli->name);
	    fclose(log_server);
	    search_client = get_client(&client_list, cli->name);

	    if (search_client == NULL) {
		client_struct = add_client(&client_list, cli->name);
		cli->accepted = 1;
	    } else {
		cli->accepted = 0;
		sendToClient("badname", cli->connfd);
		log_server = fopen("server.log", "a+");
		fprintf(log_server, "Klient se připojil s přezdívkou, která je již použita.\n");
		fclose(log_server);
		close(cli->connfd);
	    }
	    sleep(1);
	    join_game = get_game_rejoin(&game_list, cli->name);
	    if (join_game == NULL) {

	    } else {
		sleep(1);
		sendToClient(join_game->rejoin_string, cli->connfd);
	    }
	} else if (!strcmp("create", command)) {
	    log_server = fopen("server.log", "a+");
	    if (cli->name != NULL) {
		fprintf(log_server, "Klient %s zažádal o vytvoření místnosti\n", cli->name);
	    }
	    if (last_game_id >= MAX_ROOMS) {
		sendToClient("no;rooms", cli->connfd);
		fprintf(log_server, "no;rooms - Vytvoření bylo zamítnuto, na serveru je maximální počet místnost.\n");
	    } else {
		if (cli->game_state == 0) {
		    hra_struct = add_game(&game_list, cli->connfd, 0);
		    cli->game_id = hra_struct->game_id; // prirazeni hry hraci
		    strcpy(hra_struct->p1_nick, cli->name);
		    cli->leader = 1;
		    cli->game_state = 1;
		    char *youarein = malloc(sizeof (char)*20);
		    sprintf(youarein, "youarein;%d", hra_struct->game_id);
		    sendToClient(youarein, cli->connfd);
		    free(youarein);
		    join_game = get_game(&game_list, hra_struct->game_id);
		    fprintf(log_server, "Místnost číslo %d, byla vytvořena.\n", cli->game_id);

		}
	    }
	    fclose(log_server);
	} else if (!strcmp("norejoin", command)) {
	    log_server = fopen("server.log", "a+");
	    fprintf(log_server, "Klient %s odmítl připojení do jeho rozehrané hry\n", cli->name);
	    // klient se znovu připojil na server. Server mu dal najevo, že má rozehranou hru 
	    // klient ale nemá zájem ve hře pokračovat -> informuju druhého hráče, že je konec
	    if (join_game != NULL) {
		cli->game_id = 0;
		cli->game_state = 0;
		if (cli->leader == 1) {
		    // dám vědět oponentovi, místnost není jeho, bude smazána
		    sendToClient("norejoin;", join_game->p2_socket);
		    list_remove_game(list_search_game(&game_list, join_game->game_id));
		} else {
		    // dám vědět leaderovi, může čekat na jiného protihráče, udělám mu "místo"
		    sendToClient("norejoin;", join_game->p1_socket);
		    strcpy(join_game->p2_nick, "");
		}
	    }
	    join_game = get_game_null();
	    fclose(log_server);
	} else if (!strcmp("users", command)) {
	    char *buffer = malloc(sizeof (char)*100);
	    sprintf(buffer, "%s", "users;");
	    sprintf(buffer + strlen(buffer), "%d", cli_count);
	    sendToClient(buffer, cli->connfd);
	    free(buffer);

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
	} else if (!strcmp("join_o", command)) {
	    log_server = fopen("server.log", "a+");
	    command = strtok(NULL, ";");
	    int index = atoi(command);
	    join_game = get_game(&game_list, index);
	    cli->game_state = 1;
	    fprintf(log_server, "join_o - Klient %s se chce připojit do založené místnosti\n", cli->name);

	    if (join_game == NULL) {
		sendToClient("no;notexist", cli->connfd);
		fprintf(log_server, "Místnost ale neexistuje\n");

		cli->game_state = 0;
	    } else {
		fprintf(log_server, "Klient je úspěšně připojen do hry\n");

		join_game->p2_socket = cli->connfd;
		strcpy(join_game->p2_nick, cli->name);
		sendToClient("yes;", cli->connfd);
		char *lobby_data = malloc(sizeof (char)*100);
		sprintf(lobby_data, "%s;%s;%d;%s;%s;%d;%d", "game", join_game->seed, 0, join_game->p1_nick, join_game->p2_nick, join_game->p1_team, join_game->p2_team);
		sendToClient(lobby_data, join_game->p2_socket); // 0	
		sprintf(lobby_data, "%s;%s;%d;%s;%s;%d;%d", "game", join_game->seed, 1, join_game->p1_nick, join_game->p2_nick, join_game->p1_team, join_game->p2_team);
		sendToClient(lobby_data, join_game->p1_socket); // 1
		free(lobby_data);
		sendToClient("ready;", join_game->p1_socket);
		sendToClient("ready;", join_game->p2_socket);
	    }
	    fclose(log_server);
	} else if (!strcmp("join_l", command)) {
	    log_server = fopen("server.log", "a+");

	    fprintf(log_server, "Klient %s (zakladatel) se pokouší opět připojit do rozerhrané hry\n", cli->name);

	    cli->leader = 1;
	    command = strtok(NULL, ";");
	    int index = atoi(command);
	    join_game = get_game(&game_list, index);
	    cli->game_state = 1;

	    if (join_game == NULL) {
		sendToClient("no;notexist", cli->connfd);
	    } else {
		join_game->p1_socket = cli->connfd;
		strcpy(join_game->p1_nick, cli->name);
		sendToClient("yes;", cli->connfd);
		char *lobby_data = malloc(sizeof (char)*100);
		sprintf(lobby_data, "%s;%s;%d;%s;%s;%d;%d", "game", join_game->seed, 0, join_game->p1_nick, join_game->p2_nick, join_game->p1_team, join_game->p2_team);
		sendToClient(lobby_data, join_game->p2_socket); // 0	
		sprintf(lobby_data, "%s;%s;%d;%s;%s;%d;%d", "game", join_game->seed, 1, join_game->p1_nick, join_game->p2_nick, join_game->p1_team, join_game->p2_team);
		sendToClient(lobby_data, join_game->p1_socket); // 1
		free(lobby_data);
		sendToClient("ready;", join_game->p1_socket);
		sendToClient("ready;", join_game->p2_socket);
	    }
	    fclose(log_server);

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	} else if (!strcmp("room_info", command)) {
	    if (join_game == NULL) {
		sendToClient("no;no_room", cli->connfd);
		printf("Klient neni v zadne mistnosti\n");
	    } else {
		printf("___________\nInfo o lobby: \n___________\n");
		printf("%s je v mistnosti #%d\n", cli->name, join_game->game_id);
		printf("Soket p1 = (%d) \n", join_game->p1_socket);
		printf("P1_name: %s\n", join_game->p1_nick);
		printf("P1_ready: (%d)\n", join_game->p1_ready);
		printf("P1_team: (%d)\n", join_game->p1_team);
		printf("###############\n");
		printf("Soket p2 = (%d)\n", join_game->p2_socket);
		printf("P2_name: %s\n", join_game->p2_nick);
		printf("P2_ready: (%d)\n", join_game->p2_ready);
		printf("P2_team: (%d)\n", join_game->p2_team);
		printf("Seed: (%s)\n", join_game->seed);
		printf("rejoin_string= %s\n", join_game->rejoin_string);
		printf("kient status= %d\n", cli->leader);
	    }
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	} else if (!strcmp("leave", command)) {
	    log_server = fopen("server.log", "a+");

	    fprintf(log_server, "Klient %s opustil místnost\n", cli->name);

	    // klient opustil mistnost -( vzdal se )
	    if (join_game != NULL) {// mistnost existuje
		if (cli->leader == 1) {// jsem vedouci - oponenta vzdy vyhodim, smazu mistnost
		    // zakladatel opousti mistnost

		    if (join_game->p2_socket == 0) { // mistnost je prazdna
			list_remove_game(list_search_game(&game_list, join_game->game_id));
			join_game = get_game_null(); //smazu to

		    } else {// druhy hrac tam je
			sendToClient("win;", join_game->p2_socket);
			list_remove_game(list_search_game(&game_list, join_game->game_id));
			join_game = get_game_null(); //smazu to
		    }
		} else {

		    // oponent opouští místnost
		    // V mistnosti je leader, informuju ho o tom, že zustal opet sam
		    sendToClient("win;", join_game->p1_socket);

		    sprintf(join_game->p2_nick, "%s", "");

		    join_game->p2_socket = 0;

		    if (join_game->p1_socket == 0) {// zakladatel v mistnosti neni
			list_remove_game(list_search_game(&game_list, join_game->game_id));
		    }
		    join_game = get_game_null();
		}
	    }
	    fclose(log_server);
	    cli->game_id = 0;
	    cli->game_state = 0;
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	} else if (!strcmp("hbeat", command)) {
	    //puts("klient o sobe dava vedet\n");
	    ftime(&tmb);
	    int diff_sec = tmb.time - cur_tmb.time;
	    int diff_mil = tmb.millitm - cur_tmb.millitm;
	    int ping;
	    ping = ((diff_sec * 1000 - 1000) + diff_mil) + 1;

	    //cli->ping = ping; // ulozim si jeho posledni ping
	    if (join_game != NULL) {
		if (cli->leader == 1) {
		    join_game->p1_ping = ping;
		} else {
		    //puts("opponent");
		    join_game->p2_ping = ping;
		}
	    }
	    cli->ping = ping;
	    //printf("Ping %d\n", cli->ping);
	    ftime(&cur_tmb);
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	} else if (!strcmp("whoami", command)) {
	    char *whoami = malloc(sizeof (char)*100);
	    sprintf(whoami, "data;%s, soket: (%d), ping: %d ms", cli->name, cli->connfd, cli->ping);
	    sendToClient(whoami, cli->connfd);
	    free(whoami);
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	} else if (!strcmp("change_team", command)) {
	    fprintf(log_server, "change_team - žádost o změnu týmu\n");
	    if (join_game != NULL) {
		command = strtok(NULL, ";");
		int team_number = atoi(command);
		char *buffer = malloc(sizeof (char)*100);
		sprintf(buffer, "%s", "change_team;");
		sprintf(buffer + strlen(buffer), "%d", team_number);
		if (cli->leader == 1) {
		    join_game->p1_team = (join_game->p1_team + team_number);
		    if (join_game->p1_team <= 0) {
			join_game->p1_team = MAX_TEAM_ID;
		    }
		    if (join_game->p1_team > MAX_TEAM_ID) {
			join_game->p1_team = 1;
		    }
		    sendToClient(buffer, join_game->p2_socket);
		} else {
		    join_game->p2_team = (join_game->p2_team + team_number);
		    if (join_game->p2_team <= 0) {
			join_game->p2_team = MAX_TEAM_ID;
		    }
		    if (join_game->p2_team > MAX_TEAM_ID) {
			join_game->p2_team = 1;
		    }
		    sendToClient(buffer, join_game->p1_socket);
		}
		free(buffer);
	    }
	} else if (!strcmp("ready", command)) {
	    log_server = fopen("server.log", "a+");
	    fprintf(log_server, "ready - klient je připraven ke hře\n");
	    if (join_game != NULL) {
		if (cli->leader) {
		    join_game->p1_ready = 1;
		    sendToClient("op_ready;", join_game->p2_socket);

		} else {
		    join_game->p2_ready = 1;
		    sendToClient("op_ready;", join_game->p1_socket);
		}

		if (join_game->p1_ready == join_game->p2_ready) {
		    if (join_game->p1_ready == 1) {
			//oba hraci jsou pripraveni hrat
			if (join_game->p1_socket != 0) {
			    if (join_game->p2_socket) {
				sendToClient("ready;", join_game->p1_socket);
				sendToClient("ready;", join_game->p2_socket);
				fprintf(log_server, "Start hry číslo %d\n", join_game->game_id);
			    }
			}
		    }
		}
	    }
	    fclose(log_server);
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	} else if (!strcmp("rejoin_string", command)) {
	    fprintf(log_server, "rejoin_string - server přijal informace o aktuálním stavu hry\n");
	    if (join_game != NULL) {
		sprintf(join_game->rejoin_string, "%s;", "rejoin_string");
		command = strtok(NULL, ";");
		sprintf(join_game->rejoin_string + strlen(join_game->rejoin_string), "%s;", command);
		command = strtok(NULL, ";");
		sprintf(join_game->rejoin_string + strlen(join_game->rejoin_string), "%s;", command);
		command = strtok(NULL, ";");
		sprintf(join_game->rejoin_string + strlen(join_game->rejoin_string), "%s;", command);
		command = strtok(NULL, ";");
		sprintf(join_game->rejoin_string + strlen(join_game->rejoin_string), "%s", command);

	    }
	} else if (!strcmp("setdmg", command)) {
	    fprintf(log_server, "setdmg - Klient zaútočil na nepřátelský hrad\n");
	    if (join_game != NULL) {
		command = strtok(NULL, ";");
		int damage = atoi(command);
		if (cli->leader == 1) {
		    join_game->p2_hp = join_game->p2_hp - damage;
		    if (join_game->p2_hp <= 0) {
			sendToClient("win;end", join_game->p1_socket);
			sendToClient("win;end", join_game->p2_socket);
		    }
		} else {
		    join_game->p1_hp = join_game->p1_hp - damage;
		    if (join_game->p1_hp <= 0) {
			sendToClient("win;end", join_game->p1_socket);
			sendToClient("win;end", join_game->p2_socket);
		    }
		}
	    }
	} else if (!strcmp("attack", command)) {
	    if (join_game != NULL) {
		fprintf(log_server, "attack - Klient vyslal nepřátelskou jednotku\n");
		command = strtok(NULL, ";");
		char unit[1];
		strcpy(unit, command);
		char *attack_buf = malloc(sizeof (char)*100);
		int players_ping = join_game->p1_ping + join_game->p2_ping;
		if (cli->leader == 1) {
		    sprintf(attack_buf, "%s", "attack;");
		    strcpy(attack_buf + strlen(attack_buf), unit);
		    strcpy(attack_buf + strlen(attack_buf), ";");
		    sprintf(attack_buf + strlen(attack_buf), "%d", players_ping);
		    sendToClient(attack_buf, join_game->p2_socket);
		} else {
		    sprintf(attack_buf, "%s", "attack;");
		    strcpy(attack_buf + strlen(attack_buf), unit);
		    strcpy(attack_buf + strlen(attack_buf), ";");
		    sprintf(attack_buf + strlen(attack_buf), "%d", players_ping);
		    sendToClient(attack_buf, join_game->p1_socket);
		}
		free(attack_buf);
	    }
	} else {
	    fprintf(log_server, "Klient nedodržuje konvenci komunikačního protokolu, bude odpojen.\n");
	    printf("Klient nedodržuje konvenci komunikačního protokolu, bude odpojen.\n");
	    close(cli->connfd);
	}
    }
    /* Close connection */
    shutdown(cli->connfd, 1);
    if (cli->game_state == 1) {
	// vypadek - byl ve hre
	if (cli->leader == 1) {// vypadek - leader
	    if (join_game->p2_socket == 0) {// prazdna mistnost - smazat
		list_remove_game(list_search_game(&game_list, hra_struct->game_id));
	    } else {//leader ma vypadek, da to vedet oponentovi
		sendToClient("stop;", join_game->p2_socket);
		join_game->p1_socket = 0;
	    }
	} else {
	    if (join_game->p1_socket == 0) {//prazdna mistnost -smazat
		list_remove_game(list_search_game(&game_list, hra_struct->game_id));
	    } else {// opponent ma vypadek, da to vedet leaderovi
		sendToClient("stop;", join_game->p1_socket);
		join_game->p2_socket = 0;
	    }
	}
	fprintf(log_server, "stop - Příkaz k zastavení herní smyčky. Druhý klient má výpadek\n");
    }

    printf("Klient %s se odpojil od serveru\n", cli->name);
    log_server = fopen("server.log", "a+");
    fprintf(log_server, "Klient se odpojil ze serveru\n");
    fclose(log_server);

    if (cli->accepted) {
	list_remove_client(list_search_client(&client_list, cli->name));
    }


    close(cli->connfd);
    cli_count--;
    pthread_detach(pthread_self());
    cli->leader = 0;
    cli->game_state = 0;
    sprintf(cli->name, "%s", "");
    cli->connfd = -1;
    free(cli);
}

char* str_replace(const char *strbuf, const char *strold, const char *strnew) {
    char *strret, *p = NULL;
    char *posnews, *posold;
    size_t szold = strlen(strold);
    size_t sznew = strlen(strnew);
    size_t n = 1;

    if (!strbuf)
	return NULL;
    if (!strold || !strnew || !(p = strstr(strbuf, strold)))
	return strdup(strbuf);

    while (n > 0) {
	if (!(p = strstr(p + 1, strold)))
	    break;
	n++;
    }

    strret = (char*) malloc(strlen(strbuf)-(n * szold)+(n * sznew) + 1);

    p = strstr(strbuf, strold);

    strncpy(strret, strbuf, (p - strbuf));
    strret[p - strbuf] = 0;
    posold = p + szold;
    posnews = strret + (p - strbuf);
    strcpy(posnews, strnew);
    posnews += sznew;

    while (n > 0) {
	if (!(p = strstr(posold, strold)))
	    break;
	strncpy(posnews, posold, p - posold);
	posnews[p - posold] = 0;
	posnews += (p - posold);
	strcpy(posnews, strnew);
	posnews += sznew;
	posold = p + szold;
    }

    strcpy(posnews, posold);
    return strret;
}

void *handle_commands(void *arg) {
    int i;
    char command[30];
    while (1) {
	scanf("%s", command);
	if (!strcmp(command, "kill") || !strcmp(command, "quit") || !strcmp(command, "exit")) {
	    log_server = fopen("server.log", "a+");
	    fprintf(log_server, "\nServer byl očekávaně ukončen \n");
	    fprintf(log_server, "\nCelkem bylo přijato %d zpráv\n", count_received_message);
	    fprintf(log_server, "\nCelkem bylo odesláno %d zpráv\n",count_send_message );	
	    printf("\nServer byl očekávaně ukončen \n");
	    printf("Celkem bylo přijato %d zpráv\n", count_received_message);
	    printf("Celkem bylo odesláno %d zpráv\n",count_send_message );	
	    fclose(log_server);
	    exit(0);
	} else if (!strcmp(command, "rooms")) {
	    print_games_local(game_list);
	} else if (!strcmp(command, "users")) {
	    printf("Momentálně je připojeno %d klientů\n", cli_count);
	    print_clients_local(client_list);
	} else if (!strcmp(command, "help")) {
	    printf("kill, quit, exit: Ukončení běhu serveru\n");
	    printf("rooms: Vypíše všechny založené místnosti\n");
	    printf("users: Vypíše počet připojených hráčů\n");
	} else {
	    printf("Neznámý příkaz. Použijte \"help\" pro nápovědu.\n");
	}
    }
}

int main(int argc, char *argv[]) {

    count_send_message = 0;
    count_received_message = 0;
    
    
    if (argc >= 2) {
	MAX_ROOMS = atoi(argv[1]);
	PORT = atoi(argv[2]);
    } else {
	if (argc == 2) {
	    PORT = atoi(argv[2]);
	}
    }

    if (MAX_ROOMS < 1 || MAX_ROOMS > 50 || PORT == 0 || PORT > 65353 || PORT < 1024) {
	printf("Server byl spuštěn s neplatnými paramety.\nSpusťte server příkazem: ./server (počet místností) (port)\nPočet místností může být v rozsahu: 1 až 50\nČíslo portu musí být v rozsahu 1024 - 65353\n");
	exit(0);
    }

    printf("\nSpouštím server s portem: %d\n", PORT);
    printf("Počet místností na serveru: %d\n", MAX_ROOMS);

    log_server = fopen("server.log", "w");
    fclose(log_server);

    log_server = fopen("server.log", "a+");
    fprintf(log_server, "Server je spuštěn s počtem místností :%d na portu: %d\n", MAX_ROOMS, PORT);
    fclose(log_server);

    //  WSADATA wsa;
    int master, new_socket, s;
    struct sockaddr_in server, address;
    int max_clients = 30, activity, addrlen, i, valread;
    char *message = "hi-s;";

    int listenfd = 0, connfd = 0, n = 0;
    struct sockaddr_in serv_addr;
    struct sockaddr_in cli_addr;
    pthread_t tid;

    int MAXRECV = 1024;
    fd_set readfds;
    char *buffer;
    buffer = (char*) malloc((MAXRECV + 1) * sizeof (char));

    for (i = 0; i < 30; i++) {
	client_socket[i] = 0;
    }

    printf("Inicializovano.\n");

    if ((master = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
	printf("Chyba. Nemohl jsem vytvorit soket : %d", 5); //, WSAGetLastError());
	exit(1);
    }

    int optval = 1;
    setsockopt(master, SOL_SOCKET, SO_REUSEADDR, (const void *) &optval, sizeof (int));

    printf("Soket vytvořen.\n");
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);

    //Bind
    if (bind(master, (struct sockaddr *) &server, sizeof (server)) != 0) {
	log_server = fopen("server.log", "a+");
	fprintf(log_server, "Chyba při bindování soketu. Server zřejmě spouštíte znovu a nebyl předtím korektně ukončen. Zkuste to za chvíli znovu.\n");
	fclose(log_server);
	printf("Chyba při bindování soketu. Server zřejmě spouštíte znovu a nebyl předtím korektně ukončen. Zkuste to za chvíli znovu."); // WSAGetLastError());
	exit(1);
    }

    puts("Bindovani soketu probehlo v poradku");
    listen(master, 3);
    puts("Cekam na prichozi spojeni...");
    addrlen = sizeof (struct sockaddr_in);

    log_server = fopen("server.log", "a+");
    fprintf(log_server, "Server je spuštěn. Naslouchá příchozím spojením.\n");
    fclose(log_server);


    char input[100];
    pthread_create(&handle_commander, NULL, &handle_commands, NULL);

    while (1) {
	FD_ZERO(&readfds);
	FD_SET(master, &readfds);

	//add child sockets to fd set
	for (i = 0; i < max_clients; i++) {
	    s = client_socket[i];
	    if (s > 0) {
		FD_SET(s, &readfds);
	    }
	}

	if (FD_ISSET(master, &readfds)) {

	    if ((new_socket = accept(master, (struct sockaddr *) &address, (int *) &addrlen)) < 0) {
		perror("accept");
		exit(1);
	    }

	    client *cli = (client *) malloc(sizeof (client));
	    cli->addr = cli_addr;
	    cli->connfd = new_socket;
	    cli->uid = uid++;
	    cli->game_state = 0;
	    cli_count++;

	    sendToClient(message, new_socket);

	    pthread_create(&tid, NULL, &handle_client, (void*) cli);
	    sleep(1);
	}
    }//konec while
    puts("Konec serveru");

    close(s);
    return 0;
}
